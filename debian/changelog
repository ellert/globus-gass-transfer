globus-gass-transfer (9.4-3) unstable; urgency=medium

  * Enable bind-now hardening
  * Fix format warnings on 32 bit systems

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 06 Mar 2024 18:30:52 +0100

globus-gass-transfer (9.4-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062147

 -- Steve Langasek <vorlon@debian.org>  Wed, 28 Feb 2024 18:22:44 +0000

globus-gass-transfer (9.4-2) unstable; urgency=medium

  * Make doxygen Build-Depends-Indep
  * Drop old debug symbol migration from 2017

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 05 Jul 2022 21:21:22 +0200

globus-gass-transfer (9.4-1) unstable; urgency=medium

  * New GCT release v6.2.20220524
  * Drop patches included in the release

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 25 May 2022 22:13:14 +0200

globus-gass-transfer (9.3-2) unstable; urgency=medium

  * Fix some compiler and doxygen warnings

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 12 May 2022 15:08:08 +0200

globus-gass-transfer (9.3-1) unstable; urgency=medium

  * Typo fixes

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 27 Aug 2021 09:54:27 +0200

globus-gass-transfer (9.2-1) unstable; urgency=medium

  * Minor fixes to makefiles
  * Change to debhelper compat level 13
  * Remove override_dh_missing rule (--fail-missing is default)
  * Drop old symlink-to-dir conversion from 2014

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Mon, 14 Dec 2020 06:15:39 +0100

globus-gass-transfer (9.1-2) unstable; urgency=medium

  * Convert debian/rules to dh tool
  * Change to debhelper compat level 10
  * Update documentation links in README file

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 12 Jul 2019 16:34:31 +0200

globus-gass-transfer (9.1-1) unstable; urgency=medium

  * Doxygen fixes
  * Use .maintscript file for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 27 Feb 2019 20:00:50 +0100

globus-gass-transfer (9.0-1) unstable; urgency=medium

  * Switch upstream to Grid Community Toolkit
  * First Grid Community Toolkit release
  * Move VCS to salsa.debian.org

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 16 Sep 2018 03:16:31 +0200

globus-gass-transfer (8.10-2) unstable; urgency=medium

  * Migrate to dbgsym packages

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 06 Jul 2017 12:58:29 +0200

globus-gass-transfer (8.10-1) unstable; urgency=medium

  * GT6 update

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 02 Sep 2016 16:58:09 +0200

globus-gass-transfer (8.9-1) unstable; urgency=medium

  * GT6 update
  * Fix globus_gass_transfer_register_accept() not returning error when
    listener is closing or accept already registered
  * Change Maintainer e-mail (fysast → physics)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 15 Jul 2016 11:37:48 +0200

globus-gass-transfer (8.8-5) unstable; urgency=medium

  * Set SOURCE_DATE_EPOCH and rebuild using doxygen 1.8.11
    (for reproducible build)
  * Update URLs (use toolkit.globus.org)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 16 Feb 2016 13:26:05 +0100

globus-gass-transfer (8.8-4) unstable; urgency=medium

  * Rebuild using doxygen 1.8.9.1 (Closes: #630051, #630076)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 30 Apr 2015 16:47:50 +0200

globus-gass-transfer (8.8-3) unstable; urgency=medium

  * Add Pre-Depends for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 08 Nov 2014 22:48:25 +0100

globus-gass-transfer (8.8-2) unstable; urgency=medium

  * Properly handle symlink-to-dir conversion in doc package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 07 Nov 2014 11:21:21 +0100

globus-gass-transfer (8.8-1) unstable; urgency=medium

  * GT6 update
  * Drop patch globus-gass-transfer-doxygen.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 27 Oct 2014 17:02:09 +0100

globus-gass-transfer (8.7-1) unstable; urgency=medium

  * Update to Globus Toolkit 6.0
  * Drop GPT build system and GPT packaging metadata

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 24 Sep 2014 08:25:27 +0200

globus-gass-transfer (7.2-4) unstable; urgency=low

  * Remove junk man page

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 05 Dec 2013 21:08:35 +0100

globus-gass-transfer (7.2-3) unstable; urgency=low

  * Implement Multi-Arch support
  * Rename dbg package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 11 Nov 2013 07:10:44 +0100

globus-gass-transfer (7.2-2) unstable; urgency=low

  * Add arm64 to the list of 64 bit architectures

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 26 May 2013 17:57:45 +0200

globus-gass-transfer (7.2-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.1
  * Drop patch globus-gass-transfer-deps.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 29 Apr 2012 13:32:44 +0200

globus-gass-transfer (7.1-2) unstable; urgency=low

  * Fix broken links in README file

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 24 Jan 2012 19:37:24 +0100

globus-gass-transfer (7.1-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.0
  * Drop patch globus-gass-transfer-doxygen.patch (fixed upstream)
  * Make doc package architecture independent

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 28 Dec 2011 19:33:39 +0100

globus-gass-transfer (4.3-4) unstable; urgency=low

  * Use system jquery script

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 07 Jun 2011 05:44:23 +0200

globus-gass-transfer (4.3-3) unstable; urgency=low

  * Add README file
  * Use new doxygen-latex build dependency (Closes: #616225)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 26 Apr 2011 15:55:29 +0200

globus-gass-transfer (4.3-2) unstable; urgency=low

  * Converting to package format 3.0 (quilt)
  * Add new build dependency on texlive-font-utils due to changes in texlive
    packaging (epstopdf moved there) (Closes: #583030)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 04 Jun 2010 04:43:29 +0200

globus-gass-transfer (4.3-1) unstable; urgency=low

  * Update to Globus Tollkit 5.0.0
  * Add debug Package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 27 Jan 2010 09:36:36 +0100

globus-gass-transfer (3.4-6) unstable; urgency=low

  * Some additional doxygen fixes.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 14 Jun 2009 10:31:27 +0200

globus-gass-transfer (3.4-5) unstable; urgency=low

  * Fix rule dependencies in the debian/rules file.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 13 May 2009 21:55:39 +0200

globus-gass-transfer (3.4-4) unstable; urgency=low

  * Fixing inherited dependency of binary to the libgssapi-error2
    package which has now been removed from the archive.

 -- Steffen Moeller <moeller@debian.org>  Fri, 01 May 2009 10:07:20 +0200

globus-gass-transfer (3.4-3) unstable; urgency=low

  * Initial release (Closes: #514480).
  * Rebuilt to correct libltdl dependency.
  * Preparing for other 64bit platforms than amd64.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 18 Apr 2009 20:17:31 +0200

globus-gass-transfer (3.4-2) UNRELEASED; urgency=low

  * Only quote the Apache-2.0 license if necessary.
  * Updated deprecated Source-Version in debian/control.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 26 Mar 2009 09:21:25 +0100

globus-gass-transfer (3.4-1) UNRELEASED; urgency=low

  * First build.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 03 Jan 2009 15:58:43 +0100
